$(document).ready(function () {
    $("li.selected.receivertype").click(function () {
        var selectedStr=$(this).data("type");
        //把类型放入 接受人类型框
        $("input[name='receivertype']").val($(this).text());
        //type=1司机  type=2
        $("input[name='receivertype']").data("type",selectedStr);
        
    })

    $("input[name='receiver']").blur(function () {
        var receivertype= $("input[name='receivertype']").data("type");
        if(receivertype==2){
            var Passenger={};
            Passenger.passengerCitizenship=$(this).val();
            $.ajax({
                type:'POST',
                url:'/web/search/ispassenger',
                contentType:"application/json;charset=utf-8",
                DataType:"json",
                data:JSON.stringify(Passenger),
                error:function () {
                    alert("加载失败，请刷新重试");
                },
                success:function (res) {
                    if(res.data)
                    alert(res.data);
                }

            })
        }else if(receivertype==1){
            var Driver={};
            Driver.driverId=$(this).val();
            $.ajax({
                type:'POST',
                url:'/web/search/isdriver',
                contentType:"application/json;charset=utf-8",
                DataType:"json",
                data:JSON.stringify(Driver),
                error:function () {
                    alert("加载失败，请刷新重试");
                },
                success:function (res) {
                    if(res.data)
                        alert(res.data);
                }
            })
       } else {
            alert("请先选中接收人类型");
        }

    })




    $("#submit").click(function () {
        var Message={};
        Message.title=$("input[name='title']").val();
        if(Message.title==""){
            alert("请输入消息标题");
        }
        Message.receivertype=$("input[name='receivertype']").data("type");
        if(Message.receivertype==""){
            alert("请输入接收人类型");
        }
        Message.receiver=$("input[name='receiver']").val();
        if(Message.receiver==""){
            alert("请输入接收人");
        }
        Message.content=$("textarea[name='content']").val();
        if(Message.content==""){
            alert("请输入消息内容");
        }
        $.ajax({
            type:'POST',
            url:'/web/add/addmsgcontent',
            contentType:"application/json;charset=utf-8",
            DataType:"json",
            data:JSON.stringify(Message),
            error:function () {
                alert("加载失败，请刷新重试！");
            },
            success:function (res) {

            }
        })
    })


})