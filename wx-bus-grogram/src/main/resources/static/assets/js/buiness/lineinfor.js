$(document).ready(function () {
    //没有更多数据
    var noneleft="<div class='panel panel-success noneleft'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有更多数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";
    //没有数据
    var nothing="<div class='panel panel-success nothing'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";



    //初次加载 未通过审核的线路

    $('div.content.table-responsive.table-full-width').empty();
    var page=1;
    var body={startNum:page,num:10};
    $('input[type=hidden]').attr("value",page);

    $.ajax({
        type: 'POST',
        url: '/web/search/notpassroute',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON.stringify(body),
        error: function () {
            alert("加载失败，请刷新重试!");
        },
        success: function (res) {
            if(res.data.length<=0){
                $('div.content.table-responsive.table-full-width').html(nothing);
            }else {
                var more="<div class='panel panel-success more-flag0'>"+
                    "<div class='panel-heading'>" +
                    "<center>" +
                    "<h3 class='panel-title'>点击加载更多数据</h3>" +
                    "</center>" +
                    "</div>" +
                    "</div>";
                var theader="<table class='table table-striped'>" +
                    "<thead>" +
                    "<th>线路编号</th>" +
                    "<th>创建人</th>" +
                    "<th>创建时间</th>" +
                    "<th>起点站</th>" +
                    "<th>终点站</th>" +
                    "<th>出发时间</th>" +
                    "<th>预计到达</th>" +
                    "<th>运行周期</th>" +
                    "</thead>" +
                    "<tbody></tbody>" +
                    "</table>";
                $('div.content.table-responsive.table-full-width').html(theader);
                var tbody="";
                $(res.data).each(function (index,item) {
                    var creatUser=item.creatUser<0?"管理员":item.creatUser;
                    tbody+="<tr id='"+item.routeId+"'>" +
                        "<td>"+item.routeId+"</td>" +
                        "<td>"+creatUser+"</td>" +
                        "<td>"+item.creatTime+"</td>" +
                        "<td>"+item.startSite+"</td>" +
                        "<td>"+item.endSite+"</td>" +
                        "<td>"+item.startTime+"</td>" +
                        "<td>"+item.endTime+"</td>" +
                        "<td>"+item.runTime+"</td>" +
                        "</tr>";
                })
                $('tbody').append(tbody);
                if(res.data.length<10){
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }else{
                    $('div.content.table-responsive.table-full-width').append(more);
                }
            }
        }

    });

    $('li.flag_0').click(function () {
        $('div.content.table-responsive.table-full-width').empty();

        $('li.flag_0').attr("class","active flag_0");
        $('li.flag_1').attr("class","flag_1");
        $('li.flag_2').attr("class","flag_2");

        var page=1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/notpassroute',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                //如果没有返回数据
                if(res.data.length<=0){
                    $('div.content.table-responsive.table-full-width').html(nothing);
                }else {
                    var more="<div class='panel panel-success more-flag0'>"+
                        "<div class='panel-heading'>" +
                        "<center>" +
                        "<h3 id='moreWait' class='panel-title'>点击加载更多数据</h3>" +
                        "</center>" +
                        "</div>" +
                        "</div>";

                    var theader="<table class='table table-striped'>" +
                        "<thead>" +
                        "<th>线路编号</th>" +
                        "<th>创建人</th>" +
                        "<th>创建时间</th>" +
                        "<th>起点站</th>" +
                        "<th>终点站</th>" +
                        "<th>出发时间</th>" +
                        "<th>预计到达</th>" +
                        "<th>运行周期</th>" +
                        "</thead>" +
                        "<tbody></tbody>" +
                        "</table>";
                    var tbody="";
                    $('div.content.table-responsive.table-full-width').html(theader);
                    $(res.data).each(function (index,item) {
                        var creatUser=item.creatUser<0?"管理员":item.creatUser;
                        tbody+="<tr id='"+item.routeId+"'>" +
                            "<td>"+item.routeId+"</td>" +
                            "<td>"+creatUser+"</td>" +
                            "<td>"+item.creatTime+"</td>" +
                            "<td>"+item.startSite+"</td>" +
                            "<td>"+item.endSite+"</td>" +
                            "<td>"+item.startTime+"</td>" +
                            "<td>"+item.endTime+"</td>" +
                            "<td>"+item.runTime+"</td>" +
                            "</tr>";

                    })
                    $('table.table.table-striped').append(tbody);
                    if(res.data.length<10){
                        $('div.content.table-responsive.table-full-width').append(noneleft);
                    }else {
                        $('div.content.table-responsive.table-full-width').append(more);
                    }
                }
            }


        })
    });

    //flag2未通过线路加载更多
    $(document).on("click","div.panel.panel-success.more-flag0",function () {
        var page=$('input[type=hidden]').val();
        page=parseInt(page,10)+1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/notpassroute',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                var tbody="";
                $(res.data).each(function (index,item) {
                    var creatUser=item.creatUser<0?"管理员":item.creatUser;
                    tbody+="<tr id='"+item.routeId+"'>" +
                        "<td>"+item.routeId+"</td>" +
                        "<td>"+creatUser+"</td>" +
                        "<td>"+item.creatTime+"</td>" +
                        "<td>"+item.startSite+"</td>" +
                        "<td>"+item.endSite+"</td>" +
                        "<td>"+item.startTime+"</td>" +
                        "<td>"+item.endTime+"</td>" +
                        "<td>"+item.runTime+"</td>" +
                        "</tr>";
                })
                $('table.table.table-striped').append(tbody);
                if(res.data.length<10){
                    $('div.content.table-responsive.table-full-width div:last-child').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }
            }

        });

    })

    //-------------------------以下为运行中----------------------------
    $('li.flag_1').click(function () {
        $('div.content.table-responsive.table-full-width').empty();

        $('li.flag_0').attr("class","flag_0");
        $('li.flag_1').attr("class","active  flag_1");
        $('li.flag_2').attr("class","flag_2");

        var page=1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/runingroutes',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                //如果没有返回数据
                if(res.data.length<=0){
                    $('div.content.table-responsive.table-full-width').html(nothing);
                }else {
                    var more="<div class='panel panel-success more-flag1'>"+
                        "<div class='panel-heading'>" +
                        "<center>" +
                        "<h3 id='moreWait' class='panel-title'>点击加载更多数据</h3>" +
                        "</center>" +
                        "</div>" +
                        "</div>";

                    var theader="<table class='table table-striped'>" +
                        "<thead>" +
                        "<th>线路编号</th>" +
                        "<th>创建人</th>" +
                        "<th>创建时间</th>" +
                        "<th>起点站</th>" +
                        "<th>终点站</th>" +
                        "<th>出发时间</th>" +
                        "<th>预计到达</th>" +
                        "<th>运行周期</th>" +
                        "<th>绑定车辆</th>" +
                        "<th>绑定司机</th>" +
                        "</thead>" +
                        "<tbody></tbody>" +
                        "</table>";
                    var tbody="";
                    $('div.content.table-responsive.table-full-width').html(theader);
                    $(res.data).each(function (index,item) {
                        var creatUser=item.route.creatUser<0?"管理员":item.route.creatUser;
                        var bisId=item.busId==null?"未分配汽车":item.busId;
                        var driverId=item.driverId==null?"未分配司机":item.driverId;
                        tbody+="<tr id='"+item.route.routeId+"'>" +
                            "<td>"+item.route.routeId+"</td>" +
                            "<td>"+creatUser+"</td>" +
                            "<td>"+item.route.creatTime+"</td>" +
                            "<td>"+item.route.startSite+"</td>" +
                            "<td>"+item.route.endSite+"</td>" +
                            "<td>"+item.route.startTime+"</td>" +
                            "<td>"+item.route.endTime+"</td>" +
                            "<td>"+item.route.runTime+"</td>" +
                            "<td>"+bisId+"</td>" +
                            "<td>"+driverId+"</td>" +
                            "</tr>";

                    })
                    $('table.table.table-striped').append(tbody);
                    if(res.data.length<10){
                        $('div.content.table-responsive.table-full-width').append(noneleft);
                    }else {
                        $('div.content.table-responsive.table-full-width').append(more);
                    }
                }
            }


        })
    });

    //flag1运行中线路加载更多
    $(document).on("click","div.panel.panel-success.more-flag1",function () {
        var page=$('input[type=hidden]').val();
        page=parseInt(page,10)+1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/runingroutes',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                var tbody="";
                $(res.data).each(function (index,item) {
                    var creatUser=item.route.creatUser<0?"管理员":item.route.creatUser;
                    var bisId=item.busId==null?"未分配汽车":item.busId;
                    var driverId=item.driverId==null?"未分配司机":item.driverId;
                    tbody+="<tr id='"+item.route.routeId+"'>" +
                        "<td>"+item.route.routeId+"</td>" +
                        "<td>"+creatUser+"</td>" +
                        "<td>"+item.route.creatTime+"</td>" +
                        "<td>"+item.route.startSite+"</td>" +
                        "<td>"+item.route.endSite+"</td>" +
                        "<td>"+item.route.startTime+"</td>" +
                        "<td>"+item.route.endTime+"</td>" +
                        "<td>"+item.route.runTime+"</td>" +
                        "<td>"+bisId+"</td>" +
                        "<td>"+driverId+"</td>" +
                        "</tr>";
                })
                $('table.table.table-striped').append(tbody);
                if(res.data.length<10){
                    $('div.content.table-responsive.table-full-width div:last-child').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }
            }

        });
    })
    //-------------------------------已经过期------------------------------------
    $('li.flag_2').click(function () {
        $('div.content.table-responsive.table-full-width').empty();

        $('li.flag_0').attr("class","flag_0");
        $('li.flag_1').attr("class","flag_1");
        $('li.flag_2').attr("class","active flag_2");

        var page=1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/overdueroute',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                //如果没有返回数据
                if(res.data.length<=0){
                    $('div.content.table-responsive.table-full-width').html(nothing);
                }else {
                    var more="<div class='panel panel-success more-flag2'>"+
                        "<div class='panel-heading'>" +
                        "<center>" +
                        "<h3 id='moreWait' class='panel-title'>点击加载更多数据</h3>" +
                        "</center>" +
                        "</div>" +
                        "</div>";

                    var theader="<table class='table table-striped'>" +
                        "<thead>" +
                        "<th>线路编号</th>" +
                        "<th>创建人</th>" +
                        "<th>创建时间</th>" +
                        "<th>起点站</th>" +
                        "<th>终点站</th>" +
                        "<th>出发时间</th>" +
                        "<th>预计到达</th>" +
                        "<th>运行周期</th>" +
                        "</thead>" +
                        "<tbody></tbody>" +
                        "</table>";
                    var tbody="";
                    $('div.content.table-responsive.table-full-width').html(theader);
                    $(res.data).each(function (index,item) {
                        var creatUser=item.creatUser<0?"管理员":item.creatUser;
                        tbody+="<tr id='"+item.routeId+"'>" +
                            "<td>"+item.routeId+"</td>" +
                            "<td>"+item.creatUser+"</td>" +
                            "<td>"+item.creatTime+"</td>" +
                            "<td>"+item.startSite+"</td>" +
                            "<td>"+item.endSite+"</td>" +
                            "<td>"+item.startTime+"</td>" +
                            "<td>"+item.endTime+"</td>" +
                            "<td>"+item.runTime+"</td>" +
                            "</tr>";

                    })
                    $('table.table.table-striped').append(tbody);
                    if(res.data.length<10){
                        $('div.content.table-responsive.table-full-width').append(noneleft);
                    }else {
                        $('div.content.table-responsive.table-full-width').append(more);
                    }
                }
            }


        })
    });

    //flag2已经过期线路加载更多
    $(document).on("click","div.panel.panel-success.more-flag2",function () {
        var page=$('input[type=hidden]').val();
        page=parseInt(page,10)+1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/overdueroute',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                var tbody="";
                $(res.data).each(function (index,item) {
                    var creatUser=item.creatUser<0?"管理员":item.creatUser;
                    tbody+="<tr id='"+item.routeId+"'>" +
                        "<td>"+item.routeId+"</td>" +
                        "<td>"+item.creatUser+"</td>" +
                        "<td>"+item.creatTime+"</td>" +
                        "<td>"+item.startSite+"</td>" +
                        "<td>"+item.endSite+"</td>" +
                        "<td>"+item.startTime+"</td>" +
                        "<td>"+item.endTime+"</td>" +
                        "<td>"+item.runTime+"</td>" +
                        "</tr>";
                })
                $('table.table.table-striped').append(tbody);
                if(res.data.length<10){
                    $('div.content.table-responsive.table-full-width div:last-child').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }
            }

        });
    })
})